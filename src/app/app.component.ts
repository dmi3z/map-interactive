import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2
} from '@angular/core';
import { fromEvent } from 'rxjs';
import { MAP_DATA } from './map-data';
import { STATIONS_DATA } from './stations-data';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})


export class AppComponent implements OnInit {

  @ViewChild('map', { static: true })
  public canvasRef: ElementRef<HTMLCanvasElement>;

  public text = '30.207 1.310, 0.555 1.310, 0.555 18.550, 21.932 18.550, 30.207 18.550, 30.207 1.310';

  private context: CanvasRenderingContext2D;
  private areasPath: Array<{ id: number, path: Path2D }> = [];
  private stationsPath: Array<{ id: number, path: Path2D }> = [];

  private mapData = MAP_DATA;
  private stationsData = STATIONS_DATA;

  constructor(private renderer: Renderer2) { }

  ngOnInit(): void {
    const canvas = this.canvasRef.nativeElement;

    fromEvent<MouseEvent>(canvas, 'click').subscribe(res => {
      console.log({ x: res.pageX, y: res.pageY });
    });


    fromEvent<MouseEvent>(canvas, 'mousemove').subscribe(res => {

      const focusedArea = this.areasPath.find(item => {
        return this.context.isPointInPath(item.path, res.pageX, res.pageY);
      });

      const focusedStation = this.stationsPath.find(item => {
        return this.context.isPointInPath(item.path, res.pageX, res.pageY);
      });

      this.mapData.forEach(area => area.fill = '#f5a7001a');
      this.stationsData.forEach(station => station.fill = '#AAAAB1');

      if (focusedArea) {
        const element = this.mapData.find(item => item.id === focusedArea.id);
        element.fill = '#f5a70060';
      }

      if (focusedStation) {
        const element = this.stationsData.find(item => item.id === focusedStation.id);
        element.fill = 'red';
      }

      this.context.clearRect(0, 0, 700, 1000);
      this.renderMap();
    });


    this.context = canvas.getContext('2d');
    this.context.scale(1, 1);
    this.setCanvasSize({ width: 700, height: 1000 }, canvas);
    this.renderMap();
  }

  private renderMap(): void {
    this.context.moveTo(0, 0);

    this.mapData.forEach(element => {
      const path = new Path2D();
      this.setItemParams(element);
      if (element.type === 'area') {
        this.drawLine(path, element.coords);
        path.closePath();
        this.context.fillStyle = element.fill;
        this.context.fill(path);
        this.context.stroke(path);
        this.areasPath.push({ id: element.id, path });
      }
      if (element.type === 'railway') {
        this.drawRailway(element.lines);
        this.context.stroke();
      }

    });

    this.stationsData.forEach(station => {
      const path = new Path2D();
      this.setItemParams(station);
      path.arc(station.point.x, station.point.y, station.point.r, 0, 360);
      this.context.fillStyle = station.fill;
      this.context.fill(path);
      this.context.stroke(path);
      this.stationsPath.push({ id: station.id, path });
    });


    //
  }


  private drawLine(path, lines: Point[]): void {
    lines.forEach(line => {
      path.lineTo(line.x, line.y);
    });
  }

  private drawRailway(lines): void {
    lines.forEach(line => {
      this.context.moveTo(line.from.x, line.from.y);
      this.context.lineTo(line.to.x, line.to.y);
    });
  }

  private setItemParams(item): void {
    this.context.strokeStyle = item.color;
    this.context.lineWidth = item.bold;
    this.context.beginPath();
  }

  private setCanvasSize(size, canvas: HTMLCanvasElement): void {
    this.renderer.setProperty(canvas, 'width', size.width);
    this.renderer.setProperty(canvas, 'height', size.height);
  }

  public calculate(): void {
    const n = this.text.split(',');
    const strttr = n.map(item => item.trim());
    const items = strttr.map(item => item.split(' '));
    let res = '';
    const data = [];

    items.forEach(item => {
      const x = Math.round(+item[0] + 92);
      const y = Math.round(+item[1] + 491);
      data.push({ x, y });
      res += `{ x: ${x}, y: ${y} },`;
    });

    this.mapData[this.mapData.length - 1].coords = data;

    console.log(res);

  }


}


interface Point {
  x: number;
  y: number;
}
