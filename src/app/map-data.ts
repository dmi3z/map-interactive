export const MAP_DATA = [
  {
    id: 0,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Lappi',
    coords: [
      { x: 136, y: 105 }, { x: 144, y: 131 }, { x: 163, y: 131 }, { x: 172, y: 111 }, { x: 180, y: 118 }, { x: 165, y: 134 }, { x: 408, y: 134 }, { x: 361, y: 30 }, { x: 354, y: 1 }, { x: 283, y: 1 }, { x: 130, y: 1 }, { x: 130, y: 7 }, { x: 147, y: 49 }, { x: 136, y: 79 }, { x: 136, y: 105 }
    ]
  },
  {
    id: 1,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Perameri',
    coords: [
      { x: 271, y: 134 }, { x: 230, y: 227 }, { x: 200, y: 227 }, { x: 200, y: 185 }, { x: 152, y: 155 }, { x: 144, y: 130 }, { x: 162, y: 130 }, { x: 171, y: 111 }, { x: 179, y: 118 }, { x: 165, y: 134 }, { x: 271, y: 134 }
    ]
  },
  {
    id: 2,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Kainuu',
    coords: [
      { x: 435, y: 346 }, { x: 337, y: 346 }, { x: 337, y: 345 }, { x: 329, y: 345 }, { x: 310, y: 327 }, { x: 223, y: 279 }, { x: 230, y: 248 }, { x: 230, y: 228 }, { x: 271, y: 134 }, { x: 408, y: 134 }, { x: 421, y: 163 }, { x: 398, y: 163 }, { x: 404, y: 173 }, { x: 404, y: 246 }, { x: 424, y: 246 }, { x: 424, y: 267 }, { x: 421, y: 270 }, { x: 421, y: 292 }, { x: 450, y: 310 }, { x: 450, y: 327 }, { x: 435, y: 346 }
    ]
  },
  {
    id: 3,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Osu',
    coords: [
      { x: 230, y: 227 }, { x: 200, y: 227 }, { x: 200, y: 248 }, { x: 230, y: 248 }, { x: 230, y: 227 }
    ]
  },
  {
    id: 4,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Jokilaakso',
    coords: [
      { x: 230, y: 248 }, { x: 223, y: 279 }, { x: 196, y: 319 }, { x: 158, y: 319 }, { x: 158, y: 325 }, { x: 122, y: 325 }, { x: 191, y: 246 }, { x: 200, y: 246 }, { x: 200, y: 248 }, { x: 230, y: 248 }
    ]
  },
  {
    id: 5,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Sonka',
    coords: [
      { x: 223, y: 279 }, { x: 196, y: 319 }, { x: 305, y: 374 }, { x: 305, y: 414 }, { x: 359, y: 414 }, { x: 359, y: 365 }, { x: 337, y: 356 }, { x: 337, y: 345 }, { x: 329, y: 345 }, { x: 310, y: 327 }, { x: 223, y: 279 }
    ]
  },
  {
    id: 6, // *
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Pielinen',
    coords: [
      { x: 338, y: 346 }, { x: 436, y: 346 }, { x: 425, y: 363 }, { x: 509, y: 432 }, { x: 505, y: 456 }, { x: 417, y: 471 }, { x: 380, y: 415 }, { x: 359, y: 414 }, { x: 359, y: 366 }, { x: 337, y: 356 }, { x: 336, y: 345 }
    ]
  },
  {
    id: 7,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Kallio',
    coords: [
      { x: 159, y: 319 }, { x: 159, y: 340 }, { x: 179, y: 340 }, { x: 194, y: 396 }, { x: 210, y: 402 }, { x: 216, y: 414 }, { x: 305, y: 414 }, { x: 305, y: 374 }, { x: 196, y: 319 }, { x: 159, y: 319 }
    ]
  },
  {
    id: 8,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Pohjanmaa',
    coords: [
      { x: 126, y: 370 }, { x: 125, y: 328 }, { x: 155, y: 328 }, { x: 155, y: 343 }, { x: 175, y: 343 }, { x: 189, y: 393 }, { x: 144, y: 393 }
    ]
  },
  {
    id: 9,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Kokko',
    coords: [
      { x: 125, y: 328 }, { x: 125, y: 369 }, { x: 89, y: 369 }, { x: 86, y: 366 }, { x: 120, y: 328 }, { x: 125, y: 328 }
    ]
  },
  {
    id: 10,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    coords: [
      { x: 273, y: 449 }, { x: 228, y: 414 }, { x: 216, y: 414 }, { x: 210, y: 402 }, { x: 195, y: 397 }, { x: 147, y: 397 }, { x: 215, y: 491 }
    ]
  },
  {
    id: 11,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Lakeus',
    coords: [
      { x: 84, y: 370 }, { x: 55, y: 405 }, { x: 103, y: 492 }, { x: 122, y: 492 }, { x: 121, y: 447 }, { x: 152, y: 411 }, { x: 124, y: 374 }, { x: 89, y: 374 }, { x: 84, y: 370 }
    ]
  },
  {
    id: 12,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Polja',
    coords: [
      { x: 332, y: 418 }, { x: 239, y: 418 }, { x: 275, y: 445 }, { x: 330, y: 445 }, { x: 320, y: 428 }, { x: 332, y: 418 }
    ]
  },
  {
    id: 13,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Karvio',
    coords: [
      { x: 378, y: 418 }, { x: 332, y: 418 }, { x: 320, y: 428 }, { x: 330, y: 445 }, { x: 328, y: 468 }, { x: 319, y: 519 }, { x: 296, y: 541 }, { x: 338, y: 566 }, { x: 346, y: 546 }, { x: 414, y: 511 }, { x: 414, y: 473 }
    ]
  },
  {
    id: 14,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Savo',
    coords: [
      { x: 330, y: 445 }, { x: 277, y: 446 }, { x: 275, y: 489 }, { x: 283, y: 520 }, { x: 319, y: 520 }, { x: 328, y: 462 }
    ]
  },
  {
    id: 15,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Jyva',
    coords: [
      { x: 215, y: 495 }, { x: 230, y: 510 }, { x: 232, y: 533 }, { x: 224, y: 535 }, { x: 237, y: 545 }, { x: 251, y: 543 }, { x: 250, y: 540 }, { x: 286, y: 536 }, { x: 275, y: 490 }, { x: 276, y: 451 }
    ]
  },
  {
    id: 16,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Haapa',
    coords: [
      { x: 121, y: 447 }, { x: 152, y: 411 }, { x: 214, y: 494 }, { x: 230, y: 509 }, { x: 231, y: 533 }, { x: 202, y: 544 }, { x: 193, y: 565 }, { x: 195, y: 587 }, { x: 145, y: 591 }, { x: 130, y: 523 }, { x: 113, y: 510 }, { x: 122, y: 510 }, { x: 122, y: 497 }
    ]
  },
  {
    id: 17,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Pirkanmaa',
    coords: [
      { x: 202, y: 545 }, { x: 193, y: 565 }, { x: 195, y: 587 }, { x: 165, y: 590 }, { x: 165, y: 605 }, { x: 189, y: 604 }, { x: 256, y: 595 }, { x: 252, y: 543 }, { x: 238, y: 545 }, { x: 225, y: 535 }
    ]
  },
  {
    id: 18,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Pieksakmaki',
    coords: [
      { x: 286, y: 536 }, { x: 296, y: 541 }, { x: 318, y: 520 }, { x: 282, y: 520 }
    ]
  },
  {
    id: 19,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Taco',
    coords: [
      { x: 141, y: 577 }, { x: 110, y: 583 }, { x: 105, y: 589 }, { x: 101, y: 607 }, { x: 165, y: 604 }, { x: 165, y: 590 }, { x: 144, y: 591 }
    ]
  },
  {
    id: 20,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Lieska',
    coords: [
      { x: 18, y: 541 }, { x: 90, y: 583 }, { x: 108, y: 584 }, { x: 142, y: 577 }, { x: 130, y: 523 }, { x: 113, y: 510 }, { x: 105, y: 513 }, { x: 95, y: 519 }, { x: 88, y: 523 }, { x: 87, y: 527 }, { x: 91, y: 530 }, { x: 91, y: 535 }, { x: 59, y: 539 }
    ]
  },
  {
    id: 20,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Suupohja',
    coords: [
      { x: 58, y: 539 }, { x: 18, y: 541 }, { x: 18, y: 514 }, { x: 8, y: 505 }, { x: 8, y: 476 }, { x: 93, y: 501 }, { x: 93, y: 510 }, { x: 113, y: 510 }, { x: 106, y: 512 }, { x: 87, y: 523 }, { x: 87, y: 527 }, { x: 91, y: 530 }, { x: 91, y: 535 }
    ]
  },
  {
    id: 21,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Vaski',
    coords: [
      { x: 93, y: 501 }, { x: 8, y: 476 }, { x: 8, y: 459 }, { x: 55, y: 404 }, { x: 103, y: 492 }, { x: 93, y: 492 }
    ]
  },
  {
    id: 22,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Satakunta',
    coords: [
      { x: 90, y: 583 }, { x: 18, y: 541 }, { x: 20, y: 630 }, { x: 15, y: 642 }, { x: 15, y: 656 }, { x: 62, y: 656 }, { x: 93, y: 644 }, { x: 109, y: 616 }, { x: 123, y: 616 }, { x: 125, y: 606 }, { x: 102, y: 607 }, { x: 106, y: 589 }, { x: 111, y: 583 }, { x: 108, y: 584 }
    ]
  },
  {
    id: 23,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Hame',
    coords: [
      { x: 188, y: 670 }, { x: 190, y: 603 }, { x: 125, y: 606 }, { x: 123, y: 616 }, { x: 109, y: 616 }, { x: 104, y: 625 }, { x: 146, y: 653 }, { x: 151, y: 672 }
    ]
  },
  {
    id: 24,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Vakka',
    coords: [
      { x: 146, y: 653 }, { x: 104, y: 626 }, { x: 93, y: 645 }, { x: 62, y: 657 }, { x: 65, y: 694 }, { x: 67, y: 699 }, { x: 80, y: 702 }, { x: 80, y: 694 }
    ]
  },
  {
    id: 25,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Jokisuu',
    coords: [
      { x: 417, y: 474 }, { x: 504, y: 459 }, { x: 489, y: 499 }, { x: 483, y: 509 }, { x: 469, y: 501 }, { x: 448, y: 492 }, { x: 416, y: 494 }
    ]
  },
  {
    id: 26,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Karjala',
    coords: [
      { x: 421, y: 561 }, { x: 416, y: 494 }, { x: 446, y: 492 }, { x: 467, y: 501 }, { x: 483, y: 509 }, { x: 433, y: 572 }
    ]
  },
  {
    id: 27,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Saimaa',
    coords: [
      { x: 417, y: 561 }, { x: 431, y: 573 }, { x: 401, y: 613 }, { x: 368, y: 613 }, { x: 368, y: 627 }, { x: 384, y: 637 }, { x: 374, y: 649 }, { x: 358, y: 649 }, { x: 329, y: 621 }, { x: 311, y: 621 }, { x: 337, y: 568 }, { x: 342, y: 570 }, { x: 351, y: 549 }, { x: 413, y: 514 }
    ]
  },
  {
    id: 28,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Pepovesi',
    coords: [
      { x: 285, y: 538 }, { x: 255, y: 542 }, { x: 260, y: 595 }, { x: 283, y: 647 }, { x: 299, y: 646 }, { x: 337, y: 568 }
    ]
  },
  {
    id: 29,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Salpaus',
    coords: [
      { x: 261, y: 598 }, { x: 282, y: 643 }, { x: 238, y: 643 }, { x: 231, y: 646 }, { x: 227, y: 651 }, { x: 238, y: 665 }, { x: 278, y: 680 }, { x: 264, y: 689 }, { x: 274, y: 696 }, { x: 276, y: 713 }, { x: 240, y: 726 }, { x: 225, y: 711 }, { x: 225, y: 695 }, { x: 200, y: 691 }, { x: 192, y: 671 }, { x: 194, y: 606 }
    ]
  },
  {
    id: 30,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Litti',
    coords: [
      { x: 238, y: 643 }, { x: 265, y: 643 }, { x: 265, y: 651 }, { x: 268, y: 655 }, { x: 274, y: 655 }, { x: 278, y: 669 }, { x: 281, y: 669 }, { x: 278, y: 680 }, { x: 238, y: 665 }, { x: 227, y: 651 }, { x: 231, y: 646 }
    ]
  },
  {
    id: 31,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Kouvola',
    coords: [
      { x: 299, y: 647 }, { x: 283, y: 648 }, { x: 282, y: 643 }, { x: 265, y: 643 }, { x: 265, y: 651 }, { x: 268, y: 655 }, { x: 274, y: 655 }, { x: 278, y: 669 }, { x: 300, y: 669 }
    ]
  },
  {
    id: 32,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Pajari',
    coords: [
      { x: 299, y: 646 }, { x: 311, y: 621 }, { x: 329, y: 621 }, { x: 358, y: 649 }, { x: 346, y: 665 }, { x: 362, y: 665 }, { x: 351, y: 681 }, { x: 313, y: 681 }, { x: 307, y: 672 }, { x: 299, y: 664 }, { x: 299, y: 654 }
    ]
  },
  {
    id: 33,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Kymijoki',
    coords: [
      { x: 333, y: 705 }, { x: 352, y: 681 }, { x: 314, y: 681 }, { x: 308, y: 672 }, { x: 301, y: 668 }, { x: 281, y: 668 }, { x: 278, y: 679 }, { x: 264, y: 688 }, { x: 274, y: 695 }, { x: 292, y: 695 }, { x: 298, y: 705 }
    ]
  },
  {
    id: 34,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Imatra',
    coords: [
      { x: 368, y: 626 }, { x: 384, y: 637 }, { x: 401, y: 613 }, { x: 368, y: 613 }
    ]
  },
  {
    id: 35,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Vainikkala',
    coords: [
      { x: 358, y: 649 }, { x: 374, y: 649 }, { x: 362, y: 665 }, { x: 346, y: 665 }
    ]
  },
  {
    id: 36,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Kotka',
    coords: [
      { x: 292, y: 695 }, { x: 274, y: 695 }, { x: 276, y: 712 }, { x: 298, y: 705 }
    ]
  },
  {
    id: 37,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Aura',
    coords: [
      { x: 58, y: 661 }, { x: 15, y: 661 }, { x: 15, y: 692 }, { x: 28, y: 706 }, { x: 35, y: 696 }, { x: 68, y: 725 }, { x: 68, y: 737 }, { x: 97, y: 721 }, { x: 148, y: 676 }, { x: 143, y: 659 }, { x: 84, y: 696 }, { x: 84, y: 705 }, { x: 64, y: 701 }, { x: 61, y: 694 }
    ]
  },
  {
    id: 38,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Monni',
    coords: [
      { x: 190, y: 673 }, { x: 149, y: 675 }, { x: 133, y: 689 }, { x: 165, y: 689 }, { x: 176, y: 699 }, { x: 184, y: 691 }, { x: 196, y: 689 }
    ]
  },
  {
    id: 39,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Harjanne',
    coords: [
      { x: 133, y: 689 }, { x: 111, y: 708 }, { x: 141, y: 740 }, { x: 168, y: 729 }, { x: 184, y: 708 }, { x: 175, y: 699 }, { x: 164, y: 689 }
    ]
  },
  {
    id: 40,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Ranta',
    coords: [
      { x: 167, y: 750 }, { x: 167, y: 729 }, { x: 140, y: 740 }, { x: 111, y: 708 }, { x: 96, y: 721 }, { x: 101, y: 730 }, { x: 105, y: 744 }, { x: 116, y: 744 }, { x: 123, y: 762 }
    ]
  },
  {
    id: 41,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Raasepori',
    coords: [
      { x: 88, y: 751 }, { x: 95, y: 721 }, { x: 100, y: 730 }, { x: 104, y: 744 }, { x: 115, y: 744 }, { x: 122, y: 762 }, { x: 95, y: 782 }, { x: 88, y: 771 }, { x: 103, y: 751 }
    ]
  },
  {
    id: 42,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Noname_1',
    coords: [
      { x: 198, y: 695 }, { x: 196, y: 689 }, { x: 184, y: 691 }, { x: 176, y: 699 }, { x: 185, y: 708 }, { x: 171, y: 725 }, { x: 184, y: 725 }, { x: 199, y: 725 }, { x: 199, y: 711 }, { x: 207, y: 711 }, { x: 207, y: 706 }, { x: 207, y: 697 }
    ]
  },
  {
    id: 43,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Noname_2',
    coords: [
      { x: 196, y: 740 }, { x: 189, y: 725 }, { x: 170, y: 725 }, { x: 167, y: 729 }, { x: 167, y: 750 }
    ]
  },
  {
    id: 44,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Satamarata',
    coords: [
      { x: 197, y: 725 }, { x: 189, y: 725 }, { x: 196, y: 740 }, { x: 209, y: 736 }
    ]
  },
  {
    id: 45,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Olyrata',
    coords: [
      { x: 206, y: 717 }, { x: 206, y: 711 }, { x: 198, y: 711 }, { x: 198, y: 725 }, { x: 210, y: 736 }, { x: 221, y: 733 }
    ]
  },
  {
    id: 46,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Museorautatie',
    coords: [
      { x: 222, y: 699 }, { x: 206, y: 697 }, { x: 206, y: 717 }, { x: 221, y: 733 }, { x: 237, y: 727 }, { x: 222, y: 713 }
    ]
  },

  {
    id: 47,
    type: 'area',
    bold: 1,
    color: '#F4A800',
    fill: '#f5a7001a',
    title: 'Seinajoki',
    coords: [
      { x: 122, y: 492 }, { x: 93, y: 492 }, { x: 93, y: 510 }, { x: 114, y: 510 }, { x: 122, y: 510 }
    ]
  },


  {
    id: 200,
    type: 'railway',
    bold: 2,
    title: 'Lappi to Perameri',
    color: '#00AAFF',
    lines: [
      {
        from: {
          x: 160,
          y: 30
        },
        to: {
          x: 160,
          y: 145
        }
      },
      {
        from: {
          x: 160,
          y: 145
        },
        to: {
          x: 230,
          y: 80
        }
      },
      {
        from: {
          x: 230,
          y: 80
        },
        to: {
          x: 300,
          y: 80
        }
      },
      {
        from: {
          x: 300,
          y: 80
        },
        to: {
          x: 350,
          y: 20
        }
      }
    ]
  },
  {
    id: 201,
    type: 'railway',
    bold: 2,
    title: 'Perameri to Osu',
    color: '#00AAFF',
    lines: [
      {
        from: {
          x: 160,
          y: 145
        },
        to: {
          x: 210,
          y: 180
        }
      },
      {
        from: {
          x: 210,
          y: 180
        },
        to: {
          x: 210,
          y: 240
        }
      },
    ]
  },
  {
    id: 202,
    type: 'railway',
    bold: 2,
    title: 'Osu Kokko Seinajoki Tako Hame Monni Satamarata',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 210, y: 240 },
        to: { x: 212, y: 269 }
      },
      {
        from: { x: 212, y: 269 },
        to: { x: 173, y: 329 }
      },
      {
        from: { x: 173, y: 329 },
        to: { x: 157, y: 359 }
      },
      {
        from: { x: 157, y: 359 },
        to: { x: 111, y: 359 }
      },
      {
        from: { x: 111, y: 359 },
        to: { x: 110, y: 501 }
      },
      {
        from: { x: 110, y: 501 },
        to: { x: 110, y: 551 }
      },
      {
        from: { x: 110, y: 551 },
        to: { x: 134, y: 594 }
      },
      {
        from: { x: 134, y: 594 },
        to: { x: 131, y: 629 }
      },
      {
        from: { x: 131, y: 629 },
        to: { x: 178, y: 671 }
      },
      {
        from: { x: 178, y: 671 },
        to: { x: 176, y: 685 }
      },
      {
        from: { x: 176, y: 685 },
        to: { x: 190, y: 705 }
      },
      {
        from: { x: 190, y: 705 },
        to: { x: 182, y: 735 }
      },
    ]
  },


  {
    id: 203,
    type: 'railway',
    bold: 2,
    title: 'Seinajoki Vaski',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 110, y: 503 },
        to: { x: 41, y: 434 }
      },

    ]
  },

  {
    id: 204,
    type: 'railway',
    bold: 2,
    title: 'Seinajoki Suupohja',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 110, y: 503 },
        to: { x: 65, y: 530 }
      },
      {
        from: { x: 65, y: 530 },
        to: { x: 29, y: 527 }
      },

    ]
  },

  {
    id: 205,
    type: 'railway',
    bold: 2,
    title: 'Lieska Satakunta',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 112, y: 551 },
        to: { x: 49, y: 573 }
      },
      {
        from: { x: 49, y: 573 },
        to: { x: 31, y: 600 }
      },
      {
        from: { x: 31, y: 600 },
        to: { x: 53, y: 639 }
      },
      {
        from: { x: 53, y: 639 },
        to: { x: 47, y: 647 }
      },
    ]
  },

  {
    id: 205,
    type: 'railway',
    bold: 2,
    title: 'Tako Satakunta',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 136, y: 595 },
        to: { x: 111, y: 597 }
      },
      {
        from: { x: 111, y: 597 },
        to: { x: 91, y: 638 }
      },
      {
        from: { x: 91, y: 638 },
        to: { x: 26, y: 640 }
      }
    ]
  },

  {
    id: 205,
    type: 'railway',
    bold: 2,
    title: 'Hame Aura',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 147, y: 614 },
        to: { x: 61, y: 706 }
      },
      {
        from: { x: 61, y: 706 },
        to: { x: 25, y: 672 }
      }
    ]
  },

  {
    id: 206,
    type: 'railway',
    bold: 2,
    title: 'Aura Ranta Raasepori',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 61, y: 708 },
        to: { x: 104, y: 708 }
      },
      {
        from: { x: 104, y: 708 },
        to: { x: 130, y: 752 }
      },
      {
        from: { x: 130, y: 752 },
        to: { x: 100, y: 769 }
      }
    ]
  },

  {
    id: 206,
    type: 'railway',
    bold: 2,
    title: 'Ranta Harjane',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 130, y: 753 },
        to: { x: 147, y: 725 }
      },
      {
        from: { x: 147, y: 725 },
        to: { x: 163, y: 725 }
      },
      {
        from: { x: 163, y: 725 },
        to: { x: 187, y: 700 }
      }
    ]
  },

  {
    id: 206,
    type: 'railway',
    bold: 2,
    title: 'Ranta Satamarata',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 130, y: 752 },
        to: { x: 176, y: 740 }
      },
      {
        from: { x: 176, y: 740 },
        to: { x: 184, y: 734 }
      },
      {
        from: { x: 170, y: 742 },
        to: { x: 171, y: 730 }
      },
      {
        from: { x: 171, y: 730 },
        to: { x: 184, y: 729 }
      }
    ]
  },

  {
    id: 207,
    type: 'railway',
    bold: 2,
    title: 'Tako Happa Seinajoki',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 136, y: 597 },
        to: { x: 187, y: 600 }
      },
      {
        from: { x: 187, y: 600 },
        to: { x: 183, y: 513 }
      },
      {
        from: { x: 183, y: 513 },
        to: { x: 135, y: 513 }
      },
      {
        from: { x: 135, y: 513 },
        to: { x: 112, y: 504 }
      }
    ]
  },

  {
    id: 208,
    type: 'railway',
    bold: 2,
    title: 'Pirkanma Jiva Happa Lieska',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 187, y: 600 },
        to: { x: 241, y: 531 }
      },
      {
        from: { x: 241, y: 531 },
        to: { x: 185, y: 529 }
      },
      {
        from: { x: 185, y: 529 },
        to: { x: 135, y: 542 }
      },
      {
        from: { x: 135, y: 542 },
        to: { x: 112, y: 551 }
      }
    ]
  },


  {
    id: 208,
    type: 'railway',
    bold: 2,
    title: 'Jiva Korpi Kalio Ivilieska',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 241, y: 533 },
        to: { x: 241, y: 484 }
      },
      {
        from: { x: 241, y: 484 },
        to: { x: 227, y: 470 }
      },
      {
        from: { x: 227, y: 470 },
        to: { x: 222, y: 404 }
      },
      {
        from: { x: 222, y: 404 },
        to: { x: 175, y: 328 }
      }
    ]
  },

  {
    id: 208,
    type: 'railway',
    bold: 2,
    title: 'Satamarata Olyrata Museorautatie',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 187, y: 717 },
        to: { x: 199, y: 732 }
      },
      {
        from: { x: 187, y: 717 },
        to: { x: 204, y: 719 }
      },
      {
        from: { x: 204, y: 719 },
        to: { x: 211, y: 728 }
      },
      {
        from: { x: 204, y: 719 },
        to: { x: 221, y: 719 }
      }
    ]
  },


  {
    id: 208,
    type: 'railway',
    bold: 2,
    title: 'Monni Salpaus',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 177, y: 682 },
        to: { x: 196, y: 681 }
      },
      {
        from: { x: 196, y: 681 },
        to: { x: 242, y: 621 }
      }
    ]
  },

  {
    id: 209,
    type: 'railway',
    bold: 2,
    title: 'Satamarata Salpaus',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 189, y: 718 },
        to: { x: 229, y: 640 }
      }
    ]
  },

  {
    id: 210,
    type: 'railway',
    bold: 2,
    title: 'Salpaus Noname',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 227, y: 642 },
        to: { x: 225, y: 688 }
      },
      {
        from: { x: 225, y: 688 },
        to: { x: 253, y: 707 }
      }
    ]
  },

  {
    id: 211,
    type: 'railway',
    bold: 2,
    title: 'Litti Kouvola Kotka',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 229, y: 643 },
        to: { x: 245, y: 657 }
      },
      {
        from: { x: 245, y: 657 },
        to: { x: 286, y: 659 }
      },
      {
        from: { x: 286, y: 659 },
        to: { x: 287, y: 703 }
      }
    ]
  },

  {
    id: 212,
    type: 'railway',
    bold: 2,
    title: 'Juva Pieksakmaki Repovesi Kouvola',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 240, y: 530 },
        to: { x: 295, y: 530 }
      },
      {
        from: { x: 295, y: 530 },
        to: { x: 291, y: 591 }
      },
      {
        from: { x: 291, y: 591 },
        to: { x: 283, y: 602 }
      },
      {
        from: { x: 283, y: 602 },
        to: { x: 286, y: 661 }
      },
      {
        from: { x: 287, y: 661 },
        to: { x: 279, y: 652 }
      }
    ]
  },

  {
    id: 213,
    type: 'railway',
    bold: 2,
    title: 'Lallio Siilinjarvi Kuopio Pieksakmaki',
    color: '#00AAFF',
    lines: [
      {
        from: { x: 223, y: 405 },
        to: { x: 316, y: 400 }
      },
      {
        from: { x: 316, y: 400 },
        to: { x: 316, y: 460 }
      },
      {
        from: { x: 316, y: 460 },
        to: { x: 316, y: 501 }
      },
      {
        from: { x: 316, y: 501 },
        to: { x: 295, y: 531 }
      }
    ]
  },
];
