export const STATIONS_DATA = [{
  id: 50,
  type: 'station',
  bold: 1,
  title: 'Perameri',
  color: '#AAAAB1',
  fill: '#AAAAB1',
  point: {
    x: 160,
    y: 145,
    r: 7
  }
},

{
  id: 51,
  type: 'station',
  bold: 1,
  title: 'Osu',
  color: '#AAAAB1',
  fill: '#AAAAB1',
  point: {
    x: 210,
    y: 240,
    r: 7
  }
},
{
  id: 100,
  type: 'station',
  bold: 1,
  title: 'Osu',
  color: '#AAAAB1',
  fill: '#AAAAB1',
  point: {
    x: 232,
    y: 81,
    r: 7
  }
}
]
