import { WSShape } from './../types/shape';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class WSService {

  public onNewMessage = new Subject<WSShape>();

  private ws: WebSocket;

  constructor() { }

  public connect(): void {
    this.ws = new WebSocket('ws://localhost:8080/data');

    this.ws.onmessage = (message) => {
      // console.log('From WS: ', JSON.parse(message.data));
      this.onNewMessage.next(JSON.parse(message.data));
    };
  }

  public register(): void {
    this.ws.onopen = () => {
      const connectMessage = {
        id: 'joinBoard',
        lesson: 'test'
      };
      this.send(connectMessage);
    };
  }

  public send(data): void {
    const wsData = JSON.stringify(data);
    this.ws.send(wsData);
  }

  public close(): void {
    this.ws.close();
  }

}
