import { Coordinates } from './coords';

export interface WSShape {
  color: string;
  coordinates: Coordinates[];
}

export interface Dimension {
  width: number;
  height: number;
}
